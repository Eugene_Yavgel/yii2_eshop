<?php

namespace app\components;
use yii\base\Widget;
use app\models\Category;
use Yii;

class MenuWidget extends Widget {

    public $tpl; // если $tpl=>menu - меню строится в виде списка ul; $tpl=>select - меню строится в виде списка select
    public $data; // все записи категорий из БД в виде обычного массива
    public $tree; // результат работы ф-ции по построению массива дерева категорий из обычного массива
    public $menuHtml; // готовый HTML-код в зависимости от шаблона, кот-й хранится в $tpl (menu или select)

    public function init() {
        parent::init();

        if ($this->tpl === null) {
            $this->tpl = 'menu';
        }
        $this->tpl .= '.php';
    }

    public function run() {
        // получаем закэшированые данные: get cache('key')
        $menu = Yii::$app->cache->get('menu');
        if($menu) return $menu; // если меню уже есть в кєше => вернем его

        // если меню нет в кєше - формироем меню и ...
        $this->data = Category::find()->indexBy('id')->asArray()->all();
        $this->tree = $this->getTree();
        $this->menuHtml = $this->getMenuHtml($this->tree);

        // ... записываем его в кэш: set cache('key', 'val', time)
        Yii::$app->cache->set('menu', $this->menuHtml, 60);

        return $this->menuHtml;
    }

    protected function getTree() {
        $tree = [];
        foreach ($this->data as $id => &$node) {
            if (!$node['parent_id']) {
                $tree[$id] = &$node;
            } else {
                $this->data[$node['parent_id']]['childs'][$node['id']] = &$node;
            }
        }
        return $tree;
    }

    protected function getMenuHtml($tree) {
        $str = '';
        foreach ($tree as $category) {
            $str .= $this->catToTemplate($category);
        }
        return $str;
    }

    protected function catToTemplate($category) {
        ob_start();
        include __DIR__ . '/menu_tpl/' . $this->tpl;
        return ob_get_clean();
    }

}
